#!/usr/bin/env bash

set -xeuo pipefail

try_clone_checkout() {
    url="${1}"
    ref="${2}"

    dir=$(basename "${url}" | sed 's/.git$//')

    echo "${0}: ${dir}: must be built from source for Linux builds"
    if [ ! -d "${dir}" ]; then
        git clone "${url}"
        cd "${dir}"
        git checkout "${ref}"
        cd -
    else
        echo "${0}: ${dir}: already exists: skipping clone"
    fi
}

try_clone_checkout "https://github.com/openscenegraph/OpenSceneGraph.git" "OpenSceneGraph-3.4.1"
try_clone_checkout "https://github.com/tgeijten/opensim3-scone.git" "master"
try_clone_checkout "https://github.com/simbody/simbody.git" "Simbody-3.5.4"
try_clone_checkout "https://github.com/esa/pagmo2" "master"

echo "git submodules: initializing and updating"
git submodule update --init --recursive
