/*
** PagmoObjective.cpp
**
** Copyright (C) 2013-2019 Thomas Geijtenbeek and contributors. All rights
*reserved.
**
** This file is part of SCONE. For more information, see http://scone.software.
*/

#include "PagmoObjective.h"

#include "opt_tools.h"
#include "scone/core/Exception.h"
#include "scone/core/Factories.h"
#include "scone/core/Log.h"
#include "scone/core/memory_tools.h"
#include "scone/core/profiler_config.h"
#include "scone/measures/CompositeMeasure.h"
#include "scone/optimization/Params.h"
#include "spot/spot_types.h"
#include "xo/filesystem/filesystem.h"
#include <memory>
#include <numeric>
#include <pagmo/threading.hpp>

using namespace scone;

PagmoObjective::PagmoObjective( const PropNode& props,
								const path& find_file_folder )
	: ModelObjective( props, find_file_folder )
{
	// simulation objectives must have a measure
	SCONE_THROW_IF( ! model_->GetMeasure( ),
					"No Measure defined in ModelObjective" );

	INIT_PROP( props, max_duration, 1e12 );
	INIT_PROP( props, use_single_objective, false );

	// load constraints
	auto constraints = props.try_get_child( "Constraints" );
	if ( constraints != nullptr )
	{
		for ( auto& constraint : *constraints )
		{
			SCONE_ASSERT( constraint.first == "InequalityBoundaryConstraint" );
			inequality_constraints.push_back(
				InequalityBoundaryConstraint( constraint.second ) );
		}
	}

	signature_ += stringf( ".D%.0f", max_duration );

	// initialize problem
	auto composite_measure =
		dynamic_cast< CompositeMeasure* >( model_->GetMeasure( ) );
	SCONE_THROW_IF( ! composite_measure,
					"Measure should be of type CompositeMeasure" );
	num_variables = info_.dim( );
	num_objectives = composite_measure->m_Measures.size( );

	for ( int i = 0; i < num_variables; i++ )
	{
		lower_bound.push_back( info_[ i ].min );
		upper_bound.push_back( info_[ i ].max );
		mean.push_back( info_[ i ].mean );
		std.push_back( info_[ i ].std );
		// std.push_back ( ( upper_bound[ i ] - lower_bound[ i ] ) / 4 );
	}

	// determine number of equality and inequality constraints
	num_equality_constraints = 0; // for now we assume  this to be zero
	num_inequality_constraints = 0;
	if ( constraints != nullptr )
	{
		auto model = CreateModelFromParams( info_ );
		SCONE_ASSERT_MSG( model->GetFeatures( ).store_data == true,
						  "please set store_data == true inside model" );

		// dummy evaluation of model to determine constraint dimensions
		model->SetSimulationEndTime( 0.01 );
		for ( TimeInSeconds t = evaluation_step_size_;
			  ! model->HasSimulationEnded( ); t += evaluation_step_size_ )
			AdvanceSimulationTo( *model, t );

		for ( const auto& constraint : inequality_constraints )
			num_inequality_constraints +=
				constraint.GetInequalityValue( *model ).size( );
	}
}

pagmo::vector_double
PagmoObjective::fitness( const pagmo::vector_double& dv ) const
{
	// get parameters from solution and pass to the model
	spot::par_vec values;
	for ( int i = 0; i < num_variables; i++ )
		values.push_back( dv[ i ] );

	SearchPoint params( info_ );
	params.set_values( values );

	// evaluate model
	auto model = CreateModelFromParams( params );
	model->SetSimulationEndTime( GetDuration( ) );
	for ( TimeInSeconds t = evaluation_step_size_;
		  ! model->HasSimulationEnded( ); t += evaluation_step_size_ )
		AdvanceSimulationTo( *model, t );

	// get objectives
	pagmo::vector_double objectives;
	auto composite_measure =
		dynamic_cast< CompositeMeasure* >( model->GetMeasure( ) );
	SCONE_THROW_IF( ! composite_measure,
					"Measure should be of type CompositeMeasure" );
	if ( ! use_single_objective )
		for ( int i = 0; i < num_objectives; i++ )
		{
			// might cause problems if not for calculating the Pareto dominance
			SCONE_ASSERT_MSG(
				composite_measure->m_Measures[ i ]->GetMinimize( ),
				"only support objectives that are minimized" );
			objectives.push_back(
				composite_measure->m_Measures[ i ]->GetWeightedResult(
					*model ) );
		}
	else
		objectives.push_back( GetResult( *model ) );

	// get inequality constraints
	for ( const auto& constraint : inequality_constraints )
	{
		auto values = constraint.GetInequalityValue( *model );
		objectives.insert( objectives.end( ), values.begin( ), values.end( ) );
	}

	return objectives;
}

std::pair< pagmo::vector_double, pagmo::vector_double >
PagmoObjective::get_bounds( ) const
{
	return { lower_bound, upper_bound };
}

pagmo::vector_double::size_type PagmoObjective::get_nx( ) const
{
	return num_variables;
}

pagmo::vector_double::size_type PagmoObjective::get_nobj( ) const
{
	return use_single_objective ? 1 : num_objectives;
}

pagmo::vector_double::size_type PagmoObjective::get_nec( ) const
{
	return num_equality_constraints;
}
pagmo::vector_double::size_type PagmoObjective::get_nic( ) const
{
	return num_inequality_constraints;
}

pagmo::thread_safety PagmoObjective::get_thread_safety( ) const
{
	return pagmo::thread_safety::constant;
}

bool PagmoObjective::is_valid( ) const { return true; }

void PagmoObjective::AdvanceSimulationTo( Model& m, TimeInSeconds t ) const
{
	m.AdvanceSimulationTo( t );
}

TimeInSeconds PagmoObjective::GetDuration( ) const { return max_duration; }

fitness_t PagmoObjective::GetResult( Model& m ) const
{
	return m.GetMeasure( )->GetWeightedResult( m );
}

PropNode PagmoObjective::GetReport( Model& m ) const
{
	return m.GetMeasure( )->GetReport( );
}

pagmo::vector_double PagmoObjective::sample_population( ) const
{
	std::default_random_engine de( time( 0 ) );

	// create random vector
	pagmo::vector_double dv;
	for ( int i = 0; i < num_variables; i++ )
	{
		// std::cout << mean[i] << " " << std[i] << std::endl;
		std::normal_distribution< double > dist( mean[ i ], std[ i ] );
		double variable = dist( de );
		while ( variable < lower_bound[ i ] || variable > upper_bound[ i ] )
			variable = dist( de );

		dv.push_back( variable );
	}

	return dv;
}

void PagmoObjective::UpdateInfo( const spot::objective_info& info )
{
	SCONE_ASSERT( num_variables == info.dim( ) );
	info_ = info;

	lower_bound.clear( );
	upper_bound.clear( );
	mean.clear( );
	std.clear( );
	for ( int i = 0; i < num_variables; i++ )
	{
		lower_bound.push_back( info[ i ].min );
		upper_bound.push_back( info[ i ].max );
		mean.push_back( info[ i ].mean );
		std.push_back( info[ i ].std );
	}
}

PagmoObjective::InequalityBoundaryConstraint::InequalityBoundaryConstraint(
	const PropNode& props )
{
	INIT_PROP_REQUIRED( props, filter_regex );
	INIT_PROP_REQUIRED( props, range.min );
	INIT_PROP_REQUIRED( props, range.max );
	INIT_PROP( props, use_mean, false );

	// TODO properly
	// SCONE_THROW_IF( filter_regex.patterns().size() == 1,
	// 				"constraints should contain only one pattern" );
}

pagmo::vector_double
PagmoObjective::InequalityBoundaryConstraint::GetInequalityValue(
	const Model& model ) const
{
	pagmo::vector_double values;
	auto data = model.GetData( );

	// go through all channels
	bool found = false;
	for ( int i = 0; i < data.GetChannelCount( ); i++ )
	{
		// check if channel in regex
		auto name = data.GetLabels( )[ i ];
		if ( filter_regex( name ) )
		{
			found = true;
			auto series = data.GetChannelData( data.GetChannelIndex( name ) );
			// calculate largest distance from min, max
			if ( ! use_mean )
			{
				const auto [ min_value, max_value ] =
					std::minmax_element( series.begin( ), series.end( ) );
				auto min_violation = range.getMinViolation( *min_value );
				auto max_violation = range.getMaxViolation( *max_value );
				values.push_back( min_violation );
				values.push_back( max_violation );
				log::trace( "constraint: ", name,
							"\n"
							"\tmin_violation ",
							min_violation, "\tmax_violation: ", max_violation );
			} // or distance from mean
			else
			{
				auto n = series.size( );
				double mean = 0.0;
				if ( n != 0 )
					mean =
						std::accumulate( series.begin( ), series.end( ), 0.0 ) /
						n;

				auto min_violation = range.getMinViolation( mean );
				auto max_violation = range.getMaxViolation( mean );
				values.push_back( min_violation );
				values.push_back( max_violation );
				log::trace( "constraint: ", name,
							"\n"
							"\tmin_violation ",
							min_violation, "\tmax_violation: ", max_violation );
			}
		}
	}

	// handle special case if filter_regex = simulation_end_time
	if ( filter_regex( "end_time" ) )
	{
		found = true;
		auto t_end = data.Back( ).GetTime( );

		auto min_violation = range.getMinViolation( t_end );
		auto max_violation = range.getMaxViolation( t_end );
		values.push_back( min_violation );
		values.push_back( max_violation );
		log::trace( "constraint: ",
					"end_time\n"
					"\tmin_violation ",
					min_violation, "\tmax_violation: ", max_violation );
	}

	SCONE_THROW_IF( ! found,
					to_str( filter_regex ) + " does not match in data" );

	return values;
}
