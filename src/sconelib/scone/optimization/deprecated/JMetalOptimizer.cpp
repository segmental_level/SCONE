/*
** JMetalOptimizer.cpp
**
** Copyright (C) 2013-2019 Thomas Geijtenbeek and contributors. All rights
*reserved.
**
** This file is part of SCONE. For more information, see http://scone.software.
*/

#include "BinaryTournament2.h"
#include "JMetalOptimizer.h"
#include "NSGAII.h"
#include "PolynomialMutation.h"
#include "ProblemFactory.h"
#include "SBXCrossover.h"
#include "Solution.h"
#include "scone/core/Exception.h"
#include "scone/core/Factories.h"
#include "scone/core/PropNode.h"
#include "xo/filesystem/filesystem.h"
#include "xo/string/string_tools.h"
#include <memory>
#include <string>

namespace scone
{
JMetalOptimizer::JMetalOptimizer ( const PropNode& props,
                                   const PropNode& scenario_pn,
                                   const path& scenario_dir )
    : Optimizer ( props, scenario_pn, scenario_dir )
{
	SCONE_THROW_IF(max_threads > 1, "Multi-threading not supported yet.")

    INIT_PROP ( props, population_size, 100 );
    INIT_PROP ( props, distribution_index, 20 );
    INIT_PROP ( props, crossover_probability, 0.9 );

	// get a working version of the objective and do not use m_Objective
    working_objective =
        CreateObjective (
            FindFactoryProps ( GetObjectiveFactory ( ), props, "Objective" ),
            scenario_dir )
            .release ( );
}

void JMetalOptimizer::Run ( )
{
	auto problem = dynamic_cast< Problem* > ( working_objective );
	SCONE_THROW_IF ( ! problem, "Please derive from JMetalObjective" );

	// Problem is deleted by Algorithm. That is why we create a
	// working_objective, because m_Objective is a unique pointer
	// owned by Optimizer and should not be deleted.
	auto algorithm = make_unique< NSGAII > ( problem );
	algorithm->setInputParameter ( "populationSize", &population_size );
	algorithm->setInputParameter ( "maxEvaluations", &max_generations );

	// Mutation and Crossover for Real codification
	map< string, void* > parameters;
	double crossoverProbability = crossover_probability;
	double crossoverDistributionIndex = distribution_index;
	parameters[ "probability" ] = &crossoverProbability;
	parameters[ "distributionIndex" ] = &crossoverDistributionIndex;
	auto crossover = make_unique< SBXCrossover > ( parameters );

	parameters.clear ( );
	double mutationProbability = 1.0 / problem->getNumberOfVariables ( );
	double mutationDistributionIndex = distribution_index;
	parameters[ "probability" ] = &mutationProbability;
	parameters[ "distributionIndex" ] = &mutationDistributionIndex;
	auto mutation = make_unique< PolynomialMutation > ( parameters );

	// Selection Operator
	parameters.clear ( );
	auto selection = make_unique< BinaryTournament2 > ( parameters );

	// Add the operators to the algorithm
	algorithm->addOperator ( "crossover", crossover.get ( ) );
	algorithm->addOperator ( "mutation", mutation.get ( ) );
	algorithm->addOperator ( "selection", selection.get ( ) );

    // create output folder
    PrepareOutputFolder ( );

    // Execute the Algorithm
    SolutionSet* population = algorithm->execute ( );

    // write all solutions
    auto info = working_objective->info ( );
    for ( int i = 0; i < population->size ( ); i++ )
    {
        // convert solution into SearchPoint
        auto solution = population->get ( i );
        Variable** variables = solution->getDecisionVariables ( );
        spot::par_vec values;
        for ( int j = 0; j < solution->getNumberOfVariables ( ); j++ )
        {
            values.push_back ( variables[ j ]->getValue ( ) );
        }
        SearchPoint params ( info );
        params.set_values ( values );

        // write solution into file
        auto filename = GetOutputFolder ( ).str ( ) + "/";
        double total_objective = 0;
        for ( int j = 0; j < solution->getNumberOfObjectives ( ); j++ )
        {
            auto value = solution->getObjective ( j );
            total_objective += value;
            filename += to_string ( value ) + "_";
        }
        filename += to_string ( total_objective ) + ".par";
        std::ofstream str ( filename );
        str << params;
		str.close();
    }

	population->printVariablesToFile ( GetOutputFolder ( ).str ( ) + "/VAR" );
    population->printObjectivesToFile ( GetOutputFolder ( ).str ( ) + "/FUN" );
}

double JMetalOptimizer::GetBestFitness ( ) const
{
    // auto solution = population->printObjectives();
    return 0;
}

String JMetalOptimizer::GetClassSignature ( ) const
{
    auto str = Optimizer::GetClassSignature ( );
    return str;
}
} // namespace scone
