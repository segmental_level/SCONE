/*
** JMetalObjective.cpp
**
** Copyright (C) 2013-2019 Thomas Geijtenbeek and contributors. All rights
*reserved.
**
** This file is part of SCONE. For more information, see http://scone.software.
*/

#include "JMetalObjective.h"

#include "RealNormSolutionType.h"
#include "SolutionType.h"
#include "opt_tools.h"
#include "scone/core/Exception.h"
#include "scone/core/Factories.h"
#include "scone/core/Log.h"
#include "scone/core/memory_tools.h"
#include "scone/core/profiler_config.h"
#include "scone/measures/CompositeMeasure.h"
#include "scone/optimization/Params.h"
#include "spot/spot_types.h"
#include "xo/filesystem/filesystem.h"

namespace scone
{

JMetalObjective::JMetalObjective ( const PropNode& props,
                                   const path& find_file_folder )
    : ModelObjective ( props, find_file_folder )
{
    // simulation objectives must have a measure
    SCONE_THROW_IF ( ! model_->GetMeasure ( ),
                     "No Measure defined in ModelObjective" );

    INIT_PROP ( props, max_duration, 1e12 );

    signature_ += stringf ( ".D%.0f", max_duration );

    // initialize problem
    auto composite_measure =
        dynamic_cast< CompositeMeasure* > ( model_->GetMeasure ( ) );
    SCONE_THROW_IF ( ! composite_measure,
                     "Measure should be of type CompositeMeasure" );
    numberOfVariables_ = info_.dim ( );
    numberOfObjectives_ = composite_measure->m_Measures.size ( );
    numberOfConstraints_ = 0;

    lowerLimit_ = new double[ numberOfVariables_ ];
    upperLimit_ = new double[ numberOfVariables_ ];
    auto mean_ = new double[ numberOfVariables_ ];
    auto std_ = new double[ numberOfVariables_ ];
    for ( int i = 0; i < numberOfVariables_; i++ )
    {
        lowerLimit_[ i ] = info_[ i ].min;
        upperLimit_[ i ] = info_[ i ].max;
        mean_[ i ] = info_[ i ].mean;
        std_[i] = info_[i].std;
        // std_[ i ] = ( upperLimit_[ i ] - lowerLimit_[ i ] ) / 4;
    }

    // RealNormSolutionType takes ownership (frees) of mean and std
    solutionType_ = new RealNormSolutionType ( this, mean_, std_ );
    // solutionType_ = new RealSolutionType ( this );
}

JMetalObjective::~JMetalObjective ( )
{
    delete solutionType_;
    delete[] lowerLimit_;
    delete[] upperLimit_;
    // delete[] mean_;
    // delete[] std_;
}

void JMetalObjective::evaluate ( Solution* solution )
{
    // get parameters from solution and pass to the model
    Variable** variables = solution->getDecisionVariables ( );
    spot::par_vec values;
    for ( int i = 0; i < numberOfVariables_; i++ )
    {
        values.push_back ( variables[ i ]->getValue ( ) );
    }
    SearchPoint params ( info_ );
    params.set_values ( values );

    // evaluate model
    auto model = CreateModelFromParams ( params );
    model->SetSimulationEndTime ( GetDuration ( ) );
    for ( TimeInSeconds t = evaluation_step_size_;
          ! model->HasSimulationEnded ( ); t += evaluation_step_size_ )
        AdvanceSimulationTo ( *model, t );

    // get results
    auto composite_measure =
        dynamic_cast< CompositeMeasure* > ( model->GetMeasure ( ) );
    SCONE_THROW_IF ( ! composite_measure,
                     "Measure should be of type CompositeMeasure" );
    for ( int i = 0; i < numberOfObjectives_; i++ )
    {
        auto f = composite_measure->m_Measures[ i ]->GetResult ( *model );
        solution->setObjective ( i, f );
    }
}

void JMetalObjective::AdvanceSimulationTo ( Model& m, TimeInSeconds t ) const
{
    m.AdvanceSimulationTo ( t );
}
} // namespace scone
