/*
** JMetalOptimizer.h
**
** Copyright (C) 2013-2019 Thomas Geijtenbeek and contributors. All rights reserved.
**
** This file is part of SCONE. For more information, see http://scone.software.
*/

#pragma once

#include "Optimizer.h"
#include "scone/core/Exception.h"

namespace scone
{
	/// An interface with jMetalCpp library for performing multi-objective
	/// optimizations. For now the default settings use NSGAII, SBXCrossover
	/// (crossover operator), PolynomialMutation (mutation operator) and
	/// BinaryTurnament2 (selection operator). Future versions will include an
	/// interface for choosing between different algorithms and operators for
	/// mutation, etc. For now only single thread support is implemented.
	///
	/// Author: Dimitar Stanev (jimstanev@gmail.com)
	class SCONE_API JMetalOptimizer : public Optimizer
	{
	public:
		JMetalOptimizer( const PropNode& props, const PropNode& scenario_pn,
						 const path& scenario_dir );
		JMetalOptimizer( const JMetalOptimizer& ) = delete;
		JMetalOptimizer& operator=( const JMetalOptimizer& ) = delete;

		virtual void Run() override;
		virtual double GetBestFitness() const override;

		/// Population size (default 100).
		int population_size;

		/// Distribution index for the power distribution function
		/// (default 20).
		int distribution_index;

		/// Mutation probability related to the crossover operator
		/// (default 0.8).
		double crossover_probability;

	protected:
		Objective* working_objective; // no need to delete

		virtual String GetClassSignature() const override;
	};
}
