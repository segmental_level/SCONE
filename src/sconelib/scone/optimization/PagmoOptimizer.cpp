/*
** PagmoOptimizer.cpp
**
** Copyright (C) 2013-2019 Thomas Geijtenbeek and contributors. All rights
*reserved.
**
** This file is part of SCONE. For more information, see http://scone.software.
*/

#include "PagmoOptimizer.h"
#include "PagmoObjective.h"
#include "scone/core/Exception.h"
#include "scone/core/Factories.h"
#include "scone/core/Log.h"
#include "scone/core/PropNode.h"
#include "spot/objective_info.h"
#include "xo/container/prop_node_tools.h"
#include "xo/filesystem/filesystem.h"
#include "xo/string/dictionary.h"
#include "xo/string/string_tools.h"
#include <algorithm>
#include <filesystem>
#include <iostream>
#include <limits>
#include <pagmo/algorithms/cmaes.hpp>
#include <pagmo/algorithms/maco.hpp>
#include <pagmo/algorithms/moead.hpp>
#include <pagmo/algorithms/nsga2.hpp>
#include <pagmo/algorithms/nspso.hpp>
#include <pagmo/archipelago.hpp>
#include <pagmo/batch_evaluators/thread_bfe.hpp>
#include <pagmo/bfe.hpp>
#include <pagmo/islands/thread_island.hpp>
#include <pagmo/problem.hpp>
#include <pagmo/problems/unconstrain.hpp>
#include <string>
// #include <execution> (parallel for) new C++ standard can cause
// problems on Ubuntu 18.04, thus we use tbb which is used also in
// pagmo2.
#include <tbb/parallel_for.h>

typedef std::chrono::high_resolution_clock Time;
typedef std::chrono::milliseconds ms;
typedef std::chrono::duration< float > fsec;

using namespace scone;
namespace fs = std::filesystem;

template < typename T >
std::string format( T value, const int w = 6, const int n = 2 )
{
	std::ostringstream out;
	out.precision( n );
	out << std::internal << std::setw( w ) << std::setfill( '0' ) << std::fixed
		<< value;
	return out.str( );
}

xo::dictionary< PagmoOptimizer::Method >
	method_dict( { { PagmoOptimizer::Method::NSGAII, "NSGAII" },
				   { PagmoOptimizer::Method::NSPSO, "NSPSO" },
				   { PagmoOptimizer::Method::MHACO, "MHACO" },
				   { PagmoOptimizer::Method::MOEAD, "MOEAD" },
				   { PagmoOptimizer::Method::CMAES, "CMAES" } } );

PagmoOptimizer::PagmoOptimizer( const PropNode& props,
								const PropNode& scenario_pn,
								const path& scenario_dir )
	: Optimizer( props, scenario_pn, scenario_dir ),
	  best_solution( std::numeric_limits< double >::infinity( ) ),
	  should_terminate( false )
{
	INIT_PROP( props, population_size, 28 );
	INIT_PROP( props, initialization_folder, "" );
	INIT_PROP( props, use_archipelago, false );
	INIT_PROP( props, max_evolutions, 1 );
	optimization_method =
		method_dict( props.get< string >( "optimization_method", "NSGAII" ) );

	// Populate the initialization_parameter vector based on the content of the
	// initialization_folder. No need to copy these resources because they are
	// used only for initializing the population and not for evaluation.
	if ( initialization_folder != "" )
	{
		auto folder = scenario_dir / initialization_folder;
		// when evaluating the scenario file this will be ignored (dirty because
		// initialization_folder might be wrong)
		if ( fs::is_directory( folder.c_str( ) ) )
		{
			for ( auto& item :
				  fs::recursive_directory_iterator( folder.c_str( ) ) )
			{
				if ( item.path( ).extension( ) == ".par" )
				{
					auto par_file = item.path( ).generic_string( );
					SearchPoint params( GetObjective( ).info( ) );
					auto result = params.import_values( par_file );
					initialization_parameters.push_back( params );
				}
			}
		}
		else
			log::warning(
				"initialization_folder does not point to a correct directory. "
				"This warning is fine during initialization" );
	}

	// use init_file for population initialization
	if ( use_init_file && ! init_file.empty( ) )
	{
		SearchPoint params( GetObjective( ).info( ) );
		auto result = params.import_values( init_file );
		initialization_parameters.push_back( params );
	}

	// Because PagmoObjective works in SCONE through a "hack", we get access to
	// info related to the generic Objective. We do this because the info might
	// be updated by the PagmoObjective after creating the PagmoObjective. Then,
	// we replace the working_objective's info with this one.
	auto previousInfo = GetObjective( ).info( );

	// get a working version of the objective and do not use m_Objective
	working_objective = dynamic_cast< PagmoObjective* >(
		CreateObjective(
			FindFactoryProps( GetObjectiveFactory( ), props, "Objective" ),
			scenario_dir )
			.release( ) );
	SCONE_THROW_IF( ! working_objective,
					"Objective must derive from PagmoObjective." );
	working_objective->UpdateInfo( previousInfo );

	// checks
	SCONE_THROW_IF(
		optimization_method == CMAES &&
			! working_objective->use_single_objective,
		"with CMAES, please set use_single_objective in PagmoObjective" );
	SCONE_THROW_IF( use_archipelago && ( optimization_method == NSGAII ||
										 optimization_method == NSPSO ||
										 optimization_method == MHACO ),
					"please set use_archipelago = false" );
	SCONE_THROW_IF( ! use_archipelago && ( optimization_method == MOEAD ||
										   optimization_method == CMAES ),
					"please set use_archipelago = true" );
}

PagmoOptimizer::~PagmoOptimizer( ) { delete working_objective; }

void PagmoOptimizer::Run( )
{
	// initialize GUI
	auto pn = this->GetStatusPropNode( );
	pn.set( "id", GetSignature( ) );
	this->OutputStatus( std::move( pn ) );

	// select optimization method
	pagmo::thread_bfe thread_bfe;
	pagmo::algorithm algorithm;
	if ( optimization_method == NSGAII )
	{
		pagmo::nsga2 method( max_generations );
		method.set_bfe( pagmo::bfe { thread_bfe } );
		algorithm = pagmo::algorithm { method };
	}
	else if ( optimization_method == NSPSO )
	{
		pagmo::nspso method( max_generations );
		method.set_bfe( pagmo::bfe { thread_bfe } );
		algorithm = pagmo::algorithm { method };
	}
	else if ( optimization_method == MHACO )
	{
		pagmo::maco method( max_generations );
		method.set_bfe( pagmo::bfe { thread_bfe } );
		algorithm = pagmo::algorithm { method };
	}
	else if ( optimization_method == MOEAD )
	{
		pagmo::moead method( max_generations );
		algorithm = pagmo::algorithm { method };
	}
	else if ( optimization_method == CMAES )
	{
		pagmo::cmaes method( max_generations );
		algorithm = pagmo::algorithm { method };
	}
	else
		SCONE_THROW( "method not supported." );

	// check if problem contains constraints
	pagmo::problem problem;
	if ( working_objective->get_nic( ) > 0 ||
		 working_objective->get_nec( ) > 0 )
	{
		auto weights = pagmo::vector_double(
			working_objective->get_nec( ) + working_objective->get_nic( ), 1 );
		problem =
			pagmo::unconstrain { *working_objective, "weighted", weights };
	}
	else
		problem = pagmo::problem { *working_objective };

	// batch evaluation or archipelago
	if ( ! use_archipelago )
	{
		pagmo::population population { problem, thread_bfe, population_size };

		// initialize population
		InitializePopulation( population );

		for ( auto evolution = 0; evolution < max_evolutions; evolution++ )
		{
			PrepareOutputFolder( std::to_string( evolution ) );
			// auto previous_population = population;
			population = algorithm.evolve( population );
			SavePopulation( evolution, population );

			// in case of population collapse, restore previous population so
			// that we can try to re-evolve
			// if ( population.size( ) <= 1 )
			//  population = previous_population;

			// check if there is a signal from the GUI to terminate
			if ( should_terminate )
				break;
		}
	}
	else
	{
		pagmo::population population { problem, population_size };

		// it is possible that we have to initialize each island
		InitializePopulation( population );

		pagmo::archipelago archipelago { max_threads, pagmo::thread_island { },
										 algorithm, population };

		for ( int evolution = 0; evolution < max_evolutions; evolution++ )
		{
			PrepareOutputFolder( std::to_string( evolution ) );

			archipelago.evolve( );
			archipelago.wait_check( );

			// get all solutions from each island
			pagmo::population population_arch;
			for ( const auto& island : archipelago )
			{
				const auto& population_isl = island.get_population( );
				if ( population_arch.size( ) == 0 )
					population_arch = pagmo::population( population_isl );
				else
					for ( int i = 0; i < population_isl.size( ); i++ )
						population_arch.push_back(
							population_isl.get_x( )[ i ],
							population_isl.get_f( )[ i ] );
			}

			// save population
			SavePopulation( evolution, population_arch );

			// check if there is a signal from the GUI to terminate
			if ( should_terminate )
				break;
		}
	}
}

double PagmoOptimizer::GetBestFitness( ) const { return best_solution; }

bool PagmoOptimizer::interrupt( )
{
	should_terminate = true;
	return should_terminate;
}

void PagmoOptimizer::InitializePopulation( pagmo::population& population ) const
{
	// No need to initialize in case we do not provide anything.
	if ( initialization_parameters.size( ) == 0 )
	{
		log::warning("InitializePopulation skipped because "
					 "initialization_parameters is empty");
		return;
	}

	// We use of tbb library that comes with pagmo2 instead of C++
	// standard.

	// Initialize population from initial parameters. If they are less
	// than the population size, sample.
	auto init_size = initialization_parameters.size( );
	using range_t = tbb::blocked_range< decltype( population_size ) >;
	tbb::parallel_for(
		range_t( 0u, population_size ), [ & ]( const range_t& range ) {
			for ( int i = range.begin( ); i < range.end( ); ++i )
			{
				if ( i < init_size )
				{
					population.set_x(
						i, initialization_parameters[ i ].values( ) );
				}
				else
				{
					auto x = working_objective->sample_population( );
					population.set_x( i, x );
				}
			}
		} );

	// // Only with the new C++ standard
	// std::vector<int> i(population_size);
	// std::generate(i.begin(), i.end(), [n = 0] () mutable { return n++;
	// }); std::for_each(std::execution::par, std::begin(i), std::end(i),
	//               [&](int i) {
	//                   auto x = working_objective->sample_population ( );
	//                   population.set_x (i, x );
	//               });

	// // without parallelization
	// for ( int i = 0; i < population_size; i++ )
	// {
	//     auto x = working_objective->sample_population ( );
	//     population.set_x(i, x);
	// }
}

void PagmoOptimizer::SavePopulation( int evolution,
									 const pagmo::population& population ) const
{
	// measure execution time
	static auto t0 = Time::now( );
	auto t1 = Time::now( );
	fsec fs = t1 - t0;
	t0 = t1;

	// process population solutions
	auto info = working_objective->info( );
	double total_objective_best = std::numeric_limits< double >::infinity( );
	std::map< double, std::pair< std::string, int > > solutions;
	spot::search_point_vec search_vec;
	for ( int i = 0; i < population.size( ); i++ )
	{
		// convert current population solution into SearchPoint
		SearchPoint params( info );
		params.set_values( population.get_x( )[ i ] );
		search_vec.push_back( params );

		// calculate objective value and file identifier
		double total_objective = 0;
		std::string identifier = "_-";
		for ( int j = 0; j < population.get_f( )[ i ].size( ); j++ )
		{
			auto value = population.get_f( )[ i ][ j ];
			total_objective += value;
			identifier += format( value ) + "-";
		}
		identifier += "_" + format( total_objective ) + ".par";

		solutions.insert( { total_objective, { identifier, i } } );

		if ( total_objective_best > total_objective )
			total_objective_best = total_objective;
	}

	// sort solutions based on total objective
	typedef std::function< bool(
		std::pair< double, std::pair< std::string, int > >,
		std::pair< double, std::pair< std::string, int > > ) >
		Comparator;
	Comparator compFunc = []( const auto& e1, const auto& e2 ) -> bool {
		return e1.first > e2.first;
	};
	std::set< std::pair< double, std::pair< std::string, int > >, Comparator >
		solutionsSorted( solutions.begin( ), solutions.end( ), compFunc );

	// write parameters files
	int prefix = 0;
	auto mean_std = spot::compute_mean_std( search_vec );
	info.set_mean_std( mean_std.first, mean_std.second );
	for ( const auto& solution : solutionsSorted )
	{
		auto filename =
			GetOutputFolder( ) / format( prefix, 3, 0 ) + solution.second.first;
		SearchPoint params( info );
		params.set_values( population.get_x( )[ solution.second.second ] );
		std::ofstream str( filename.c_str( ) );
		str << params;
		str.close( );
		prefix++;
	}

	// transmit results to GUI
	auto pn = this->GetStatusPropNode( );
	pn.set( "id", GetSignature( ) );
	pn.set( "folder", GetOutputFolder( ) );
	pn.set( "dim", info.dim( ) );
	pn.set( "sigma", 0 );
	pn.set( "lambda", 0 );
	pn.set( "mu", 0 );
	pn.set( "max_generations", max_generations );
	pn.set( "minimize", info.minimize( ) );
	pn.set( "window_size", 500 );
	pn.set( "step", evolution );
	pn.set( "step_best", total_objective_best );
	pn.set( "step_median", 0 );
	pn.set( "trend_offset", 0 );
	pn.set( "trend_slope", 0 );
	pn.set( "progress", 0 );
	pn.set( "time", fs.count( ) );
	pn.set( "number_of_evaluations", max_generations * evolution );
	pn.set( "evaluations_per_sec", max_generations * evolution / fs.count( ) );
	if ( best_solution > total_objective_best )
	{
		best_solution = total_objective_best;
		pn.set( "best", total_objective_best );
		pn.set( "best_gen", evolution );
	}
	if ( should_terminate || evolution == max_evolutions - 1 )
		pn.set( "finished", "" );

	this->OutputStatus( std::move( pn ) );
}
