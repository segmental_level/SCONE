/*
** JMetalObjective.h
**
** Copyright (C) 2013-2019 Thomas Geijtenbeek and contributors. All rights reserved.
**
** This file is part of SCONE. For more information, see http://scone.software.
*/

#pragma once

#include "scone/measures/CompositeMeasure.h"
#include "scone/optimization/ModelObjective.h"
#include "scone/model/Model.h"
#include "scone/core/Factories.h"
#include "Problem.h"
#include <memory>

namespace scone
{
	/// An optimization objective for interfacing with the jMetal library.
	///
	/// Author: Dimitar Stanev (jimstanev@gmail.com)
	class SCONE_API JMetalObjective : public ModelObjective, public Problem
	{
	public:
		JMetalObjective( const PropNode& props, const path& find_file_folder );
		~JMetalObjective();

		/// Maximum duration after which the evaluation is terminated; default =
		/// 1e12 (+/-31000 years)
		double max_duration;

		// Problem (jMetal) interface
		virtual void evaluate(Solution * solution) override;

		// ModelObjective interface
		virtual void AdvanceSimulationTo( Model& m, TimeInSeconds t ) const override;
		virtual TimeInSeconds GetDuration() const override { return max_duration; }
		virtual fitness_t GetResult( Model& m ) const override {
			return m.GetMeasure()->GetWeightedResult( m );
		}
		virtual PropNode GetReport( Model& m ) const override {
			return m.GetMeasure()->GetReport();
		}
	/* private: */
	/* 	double* mean_; */
	/* 	double* std_; */
	};
}
