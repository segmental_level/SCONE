/*
** PagmoObjective.h
**
** Copyright (C) 2013-2019 Thomas Geijtenbeek and contributors. All rights
*reserved.
**
** This file is part of SCONE. For more information, see http://scone.software.
*/

#pragma once

#include "scone/core/Factories.h"
#include "scone/measures/CompositeMeasure.h"
#include "scone/model/Model.h"
#include "scone/optimization/ModelObjective.h"
#include "xo/string/pattern_matcher.h"
#include <memory>
#include <pagmo/problem.hpp>
#include <pagmo/types.hpp>

namespace scone
{
	/// Implements a wrapper of SCONE's objectives into pagmo::problem.
	///
	/// We can cast the problem into a single or multi-objective optimization.
	/// Most of the algorithms supported by pagmo solve an unconstrained
	/// optimization. Here, we have introduced a mechanism to support inequality
	/// constraints using the penalty method.The user can add limits on
	/// calculated quantities such as muscle excitations. Also, we can add a
	/// constraint on the simulation end time. The last checks if the end time
	/// deviates from the min/max bounds (example below checks if end_time =
	/// 10s). If there is deviation then the penalty is distributed to all
	/// objectives.
	///
	/// \verbatim
	///	Constraints {
	///	  InequalityBoundaryConstraint {
	///		filter_regex = "*.excitation"
	///		min_value = 0.0
	///		max_value = 1.0
	///	  }
	///	  InequalityBoundaryConstraint {
	///		filter_regex = "end_time"
	///		range.min = 10.0
	///		range.max = 10.0
	///	  }
	///	}
	/// \endverbatim
	///
	/// Author: Dimitar Stanev (jimstanev@gmail.com)
	class SCONE_API PagmoObjective : public ModelObjective
	{
	public:
		/// Maximum duration after which the evaluation is terminated; default =
		/// 1e12 (+/-31000 years)
		double max_duration;

		/// Set to true in case you want to use single-objective optimization
		/// (default false).
		bool use_single_objective;

		PagmoObjective( const PropNode& props, const path& find_file_folder );

		/// Interface with pagmo.
		pagmo::vector_double fitness( const pagmo::vector_double& dv ) const;
		std::pair< pagmo::vector_double, pagmo::vector_double >
		get_bounds( ) const;
		pagmo::vector_double::size_type get_nx( ) const;
		pagmo::vector_double::size_type get_nobj( ) const;
		pagmo::vector_double::size_type get_nec( ) const;
		pagmo::vector_double::size_type get_nic( ) const;
		pagmo::thread_safety get_thread_safety( ) const;
		bool is_valid( ) const;

		/// ModelObjective interface (we do not use this for multi-objective,
		/// but this )
		virtual void AdvanceSimulationTo( Model& m,
										  TimeInSeconds t ) const override;
		virtual TimeInSeconds GetDuration( ) const override;
		virtual fitness_t GetResult( Model& m ) const override;
		virtual PropNode GetReport( Model& m ) const override;

		/// Public interface.
		pagmo::vector_double sample_population( ) const;

		/// Update initial lower and upper bounds, mean and std of the initial
		/// parameters.
		void UpdateInfo( const spot::objective_info& info );

	protected:
		pagmo::vector_double lower_bound, upper_bound, mean, std;
		int num_variables, num_objectives;
		int num_equality_constraints, num_inequality_constraints;

		/// Mechanism to handle inequality constraints using penalty method.
		class InequalityBoundaryConstraint
		{
		public:
			xo::pattern_matcher filter_regex;
			bool use_mean;

			struct range_violation
			{
				double min;
				double max;
				double getMinViolation( double value ) const
				{
					return min - value;
				}
				double getMaxViolation( double value ) const
				{
					return value - max;
				}
			} range;

			InequalityBoundaryConstraint( const PropNode& props );
			pagmo::vector_double GetInequalityValue( const Model& model ) const;
		};
		std::vector< InequalityBoundaryConstraint > inequality_constraints;
	};
} // namespace scone

/// A hack to baypass compile time issues for casting scone::PagmoObjective as
/// pagmo's user defined problem (udp).
template <>
struct pagmo::detail::disable_udp_checks< scone::PagmoObjective >
	: std::true_type
{
};
