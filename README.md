# Welcome to SCONE!

This is a custom version of SCONE which provides a syntax for building
physiological [segmental level
controllers](src/sconelib/scone/controllers/SegmentalController.h). It
also interfaces with an [multi-objective
optimizers](src/sconelib/scone/optimization/PagmoOptimizer.h) that
relies on pagmo2 library.


## Building SCONE

You can build this version of SCONE only on Ubuntu 18.04 use the
[automatic building script](build_ubuntu18.04). This script build all
the dependencies including pagmo2 multi-optimization solver. Building
this version on Windows (we have done it) is also possible but
requires a lot of effort.

## License

SCONE is licensed under the [GNU Public License
3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)
