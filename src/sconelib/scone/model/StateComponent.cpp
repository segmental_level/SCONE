/*
** StateComponent.cpp
**
** Copyright (C) 2013-2019 Thomas Geijtenbeek and contributors. All rights
*reserved.
**
** This file is part of SCONE. For more information, see http://scone.software.
*/

#include "StateComponent.h"
#include "scone/model/Model.h"

namespace scone
{
	StateComponent::StateComponent( const PropNode& props, Params& par,
									Model& model )
		: m_model( model )
	{
	}

	String StateComponent::GetStateName( int i ) const
	{
		return GetStateNames( )[ i ];
	}

	/// Retrieve state values of this component.
	std::vector< Real > StateComponent::GetStateValues( Real t ) const
	{
		std::vector< Real > state;
		for ( int i = 0; i < GetInitialConditions( ).size( ); i++ )
			state.push_back(
				m_model.GetState( ).GetValue( GetStateName( i ) ) );

		return state;
	}
} // namespace scone
