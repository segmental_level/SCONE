/*
** PagmoOptimizer.h
**
** Copyright (C) 2013-2019 Thomas Geijtenbeek and contributors. All rights
*reserved.
**
** This file is part of SCONE. For more information, see http://scone.software.
*/

#pragma once

#include "Optimizer.h"
#include <pagmo/population.hpp>

namespace scone
{
	/// forward declaration
	class PagmoObjective;

	/// An interface with the pagmo2 (https://github.com/esa/pygmo2)
	/// optimization library.
	///
	/// Author: Dimitar Stanev (jimstanev@gmail.com)
	class SCONE_API PagmoOptimizer : public Optimizer
	{
	public:
		/// max_generations is used internally. It specifies the number of
		/// evaluations required to calculate the final populations.

		/// max_threads is used in archipelago (default = -1).

		/// Population size (default 28).
		long unsigned int population_size;

		/// Initialize the population using .par files located in a folder
		/// (default = ""). If we provide the init_file and use_init_file =
		/// true, then we include this into the initialization_parameters.
		path initialization_folder;

		/// NSGAII, NSPSO, MHACO can be used with batch evaluations. MOEAD and
		/// CMAES with archipelago in order to run in parallel (use_archipelago
		/// = true).
		enum Method
		{
			NSGAII,
			NSPSO,
			MHACO,
			MOEAD,
			CMAES
		};

		/// Select the optimization method (default NSGAII).
		Method optimization_method;

		/// Use archipelago (default = false -> batch evaluation).
		bool use_archipelago;

		/// The maximum number of evolutions performed (default 1). After each
		/// evolution, the population is exported. This is useful for
		/// monitoring. max_generations should be carefully selected in order to
		/// save the solutions more frequently.
		long unsigned int max_evolutions;

		PagmoOptimizer( const PropNode& props, const PropNode& scenario_pn,
						const path& scenario_dir );
		virtual ~PagmoOptimizer( );

		/// Optimizer interface
		virtual void Run( ) override;
		virtual double GetBestFitness( ) const override;

		/// Terminate the optimization after the end of evolve.
		virtual bool interrupt( );

	protected:
		PagmoObjective* working_objective;
		std::vector< SearchPoint > initialization_parameters;
		mutable double best_solution;
		bool should_terminate;

		void InitializePopulation( pagmo::population& population ) const;
		void SavePopulation( int evolution,
							 const pagmo::population& population ) const;
	};
} // namespace scone
