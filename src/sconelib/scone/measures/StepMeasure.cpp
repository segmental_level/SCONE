/*
** StepMeasure.cpp
**
** Copyright (C) 2013-2019 Thomas Geijtenbeek and contributors. All rights
*reserved.
**
** This file is part of SCONE. For more information, see http://scone.software.
*/

#include "StepMeasure.h"
#include "scone/core/Exception.h"
#include "scone/core/GaitCycle.h"
#include "scone/core/Log.h"
#include "scone/core/profiler_config.h"
#include "scone/model/Model.h"

namespace scone
{
	StepMeasure::StepMeasure( const PropNode& props, Params& par,
							  const Model& model, const Location& loc )
		: Measure( props, par, model, loc )
	{
		INIT_PROP( props, termination_height, 0.5 );
		INIT_PROP( props, stride_length, RangePenalty< Real >( ) );
		INIT_PROP( props, stride_duration, RangePenalty< Real >( ) );
		INIT_PROP( props, stride_velocity, RangePenalty< Real >( ) );
		INIT_PROP( props, load_threshold, 0.01 );
		INIT_PROP( props, min_stance_duration_threshold, 0.2 );
		INIT_PROP( props, initiation_cycles, 1 );
		INIT_PROP( props, penalize_falling, false );
		INIT_PROP( props, reward_stay_alive, false );
		INIT_PROP( props, reward_forward_movement, false );

		SCONE_THROW_IF( initiation_cycles < 1,
						"initiation_cycles should be >= 1" );
		SCONE_THROW_IF( stride_length.IsNull( ) && stride_duration.IsNull( ) &&
						stride_velocity.IsNull( ),
						"Any of stride_length / stride_duration / "
						"stride_velocity should be defined" );

		m_InitialComPos = model.GetComPos( );
	}

	bool StepMeasure::UpdateMeasure( const Model& model, double timestamp )
	{
		SCONE_PROFILE_FUNCTION( model.GetProfiler( ) );

		auto& frame = m_storedData.AddFrame( timestamp );
		for ( const auto& leg : model.GetLegs( ) )
		{
			Vec3 force, moment, cop;
			leg->GetContactForceMomentCop( force, moment, cop );
			Vec3 grf = force / model.GetBW( );
			frame.SetVec3( leg->GetName( ) + ".grf_norm", grf );
			frame.SetVec3( leg->GetName( ) + ".cop", cop );
		}

		// check termination
		auto com_height = model.GetComHeight( );
		if ( com_height < termination_height * m_InitialComPos.y )
			return true;

		return false;
	}

	double StepMeasure::ComputeResult( const Model& model )
	{
		auto cycles = ExtractGaitCycles( m_storedData, load_threshold,
										 min_stance_duration_threshold );

		// calculate stride length / duration / velocity
		for ( index_t idx = initiation_cycles; idx < cycles.size( ); ++idx )
		{
			auto t = cycles[ idx ].begin_;
			if ( ! stride_length.IsNull( ) )
				stride_length.AddSample( t, cycles[ idx ].length( ) );
			if ( ! stride_duration.IsNull( ) )
				stride_duration.AddSample( t, cycles[ idx ].duration( ) );
			if ( ! stride_velocity.IsNull( ) )
				stride_velocity.AddSample( t, cycles[ idx ].velocity( ) );
		}

		// calculate penalty
		double penalty = 0;
		if ( ! stride_length.IsNull( ) )
		{
			if ( stride_length.IsEmpty( ) )
				stride_length.AddSample( 1, 0 );
			penalty += stride_length.GetResult( );
			GetReport( ).set( "stride_length_penalty",
							  stride_length.GetResult( ) );
		}
		if ( ! stride_duration.IsNull( ) )
		{
			if ( stride_duration.IsEmpty( ) )
				stride_duration.AddSample( 1, 0 );
			penalty += stride_duration.GetResult( );
			GetReport( ).set( "stride_duration_penalty",
							  stride_duration.GetResult( ) );
		}
		if ( ! stride_velocity.IsNull( ) )
		{
			if ( stride_velocity.IsEmpty( ) )
				stride_velocity.AddSample( 1, 0 );
			penalty += stride_velocity.GetResult( );
			GetReport( ).set( "stride_velocity_penalty",
							  stride_velocity.GetResult( ) );
		}

		// if the model is going to fall (termination_height * 1.1) then return
		// +inf to discourage unstable gaits. Useful, for multi-objective
		// optimization.
		auto com_height = model.GetComHeight( );
		if ( penalize_falling &&
			 com_height <  termination_height * 1.05 * m_InitialComPos.y )
		{		
			penalty = xo::constantsd::infinity( );
			GetReport( ).set( "penalize_falling", xo::constantsd::infinity( ) );
		}

		// Compare the simulation time and the end time. If model
		// managed to reach end time then this becomes zero.
		if ( reward_stay_alive)
		{
			double stay_alive_penalty = abs(model.GetSimulationEndTime() - model.GetTime());
			penalty += stay_alive_penalty;
			GetReport( ).set( "stay_alive_penalty", stay_alive_penalty );
		}

		// Reward if forward movement.
		if ( reward_forward_movement )
		{
			auto tt = model.GetSimulationEndTime();
			double forward_movement_penalty =
				std::max( std::min( tt, 1 / abs( model.GetComPos( ).x -
												  m_InitialComPos.x ) ),
						  1 / tt ) - 1 / tt ;
			penalty += forward_movement_penalty;
			GetReport( ).set( "forward_movement_penalty", forward_movement_penalty );
		}

		return penalty;
	}

	String StepMeasure::GetClassSignature( ) const { return stringf( "SM" ); }
} // namespace scone
