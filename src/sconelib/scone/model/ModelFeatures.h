#pragma once

namespace scone
{
	struct ModelFeatures
	{
		bool allow_external_forces = false;
		bool store_data = false;
	};
}
