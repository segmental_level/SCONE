/*
** SegmentalController.h
**
** Copyright (C) 2013-2019 Thomas Geijtenbeek and contributors. All rights
*reserved.
**
** This file is part of SCONE. For more information, see http://scone.software.
*/

#pragma once

#include "scone/controllers/Controller.h"
#include "scone/core/Bezier.h"
#include "scone/core/PieceWiseConstantFunction.h"
#include "scone/core/PropNode.h"
#include "scone/core/types.h"
#include "scone/model/Side.h"
#include "scone/model/StateComponent.h"

#include <algorithm>
#include <map>
#include <memory>
#include <sstream>
#include <stdexcept>
#include <vector>

namespace scone
{

	/// \brief Definition of a physiological spinal cord model, created by
	/// Dimitar Stanev (dimitar.stanev@epfl.ch).
	///
	/// In this implementation, it is assumed that the spinal cord is composed
	/// of multiple spinal cord segments (e.g., L1, L2, S2, etc.). Each spinal
	/// cord segment contains different types of neurons. Here we assume
	/// sensory, inter and motor neuron types. A muscle transmits sensory
	/// information to the spinal cord through afferents that can innervates
	/// multiple segments. Also, a muscle receives neural excitation from more
	/// than one spinal cord segments. The organization of the spinal cord
	/// segments is defined by two things: 1) the muscle organization
	/// specifications, and 2) reflex rules. The muscle organization specifies
	/// muscle dependent information such as antagonist or synergist muscles and
	/// the corresponding spinal cord segments. The reflex rules define the type
	/// of mono/polysynaptic pathways that exist in the spinal cord. Both of
	/// these specifications are defined by the user of this controller. The
	/// controller translates this into a network of interconnected neurons,
	/// which is used to control the muscles in SCONE.
	namespace spinal_cord
	{

		/// Helper macro used to creates enums that can be converted to string
		/// through the << operator of to_string function.
#define MAKE_ENUM( name, ... )                                                 \
	enum name                                                                  \
	{                                                                          \
		__VA_ARGS__                                                            \
	};                                                                         \
	inline std::ostream& operator<<( std::ostream& os, name value )            \
	{                                                                          \
		std::string enumName = #name;                                          \
		std::string str = #__VA_ARGS__;                                        \
		int len = str.length( );                                               \
		std::vector< std::string > strings;                                    \
		std::ostringstream temp;                                               \
		for ( int i = 0; i < len; i++ )                                        \
		{                                                                      \
			if ( isspace( str[ i ] ) )                                         \
				continue;                                                      \
			else if ( str[ i ] == ',' )                                        \
			{                                                                  \
				strings.push_back( temp.str( ) );                              \
				temp.str( std::string( ) );                                    \
			}                                                                  \
			else                                                               \
				temp << str[ i ];                                              \
		}                                                                      \
		strings.push_back( temp.str( ) );                                      \
		os << strings[ static_cast< int >( value ) ];                          \
		return os;                                                             \
	}                                                                          \
	inline string to_string( const name& value )                               \
	{                                                                          \
		std::ostringstream oss;                                                \
		oss << value;                                                          \
		return oss.str( );                                                     \
	}

		// avoid including std namespace
		using std::map;
		using std::pair;
		using std::shared_ptr;
		using std::string;
		using std::tuple;
		using std::vector;
		using std::weak_ptr;

		/***********************************************************************/
		// Define the type system of the spinal cord model.
		/***********************************************************************/

		/// Base class for symbols (= strings in our case).
		struct symbol_t : public string
		{
			using string::string;
		};

		/// Spinal cord segment type (e.g., L1, L2, S1, etc.).
		struct segment_t : public symbol_t
		{
			using symbol_t::symbol_t;
		};

		/// Muscle name type (e.g., soleus, tib_ant, gastroc, etc.).
		struct muscle_t : public symbol_t
		{
			using symbol_t::symbol_t;
		};

		/// Neuron types supported in this implementation. SN stands for sensory
		/// neurons; Ia, Ib, II are suffixes for primary and secondary
		/// afferents; IN denotes interneurons of particular types defined by
		/// the suffix; MN is a motor neuron. Future implementation may support
		/// more types of neurons.
		struct neuron_type_t : public symbol_t
		{
			using symbol_t::symbol_t;
		};

		/// A type defined tuple, that contains information about the muscle
		/// organization. For example, muscle information can be defined in
		/// muscle_organization_t as {soleus [gastroc] [tib_ant] [L5 S1 S2]
		/// true} for each muscle. This information is then used in combination
		/// with the reflex rules (defined below) to create the neural network.
		///
		/// Layout: [   0   ,    1      ,    2       ,   3     ,    4    ,   5 ]
		///         [agonist, synergists, antagonists, segments, extensor,
		///         exclude neuron]
		typedef tuple< muscle_t, vector< muscle_t >, vector< muscle_t >,
					   vector< segment_t >, bool, vector< neuron_type_t > >
			muscle_info_t;

		/// A container for muscle_info_t used to define the layout of the
		/// spinal cord and should be provided by the user of the controller.
		typedef vector< muscle_info_t > muscle_organization_t;

		/// Neurons are identified by their muscle and neuron type (e.g.,
		/// tib_ant.SN_Ib -> tibialis anterior Ib sensor neuron) and is used for
		/// lookup operations.
		typedef pair< muscle_t, neuron_type_t > neuron_identifier_t;

		/// Muscle role tag.
		MAKE_ENUM( muscle_role_t, agonist, antagonist, synergist, extensor );

		/// Neuron synapse type tag.
		MAKE_ENUM( neuron_action_t, excite, inhibit, excite_or_inhibit,
				   positive_pass_through, negative_pass_through );

		/// The definition of a reflex rule. One can define rules such as:
		/// {agonist SN_Ia excite agonist MN}. A collection of these rules (e.g.
		/// reflex_rules_t) is used to generate the neural network.
		///
		/// [     0     ,      1       ,   2   ,         3       ,        4 ]
		/// [source role, source neuron, action, destination role, destination
		/// neuron]
		typedef tuple< muscle_role_t, neuron_type_t, neuron_action_t,
					   muscle_role_t, neuron_type_t >
			rule_structure_t;

		/// A container of reflex rule.
		typedef vector< rule_structure_t > reflex_rules_t;

		/// forward declaration of a neuron type
		struct neuron_t;

		/// Definition of a synapse that connects two neurons.
		struct synapse_t
		{
			double w;
			weak_ptr< neuron_t > source, destination;
			synapse_t( double w, shared_ptr< neuron_t >& source,
					   shared_ptr< neuron_t >& destination );
		};

		/// Synapse identifier [source - destination] used for lookup functions.
		typedef pair< neuron_identifier_t, neuron_identifier_t >
			synapse_identifier_t;

		/// Base-class neuron.
		struct neuron_t
		{
			neuron_t( Model& model );
			/// Register a synapse in order to automatically resolve the
			/// propagation of the network.
			void register_synapse( shared_ptr< synapse_t >& synapse );
			/// Get input from connected neurons.
			virtual double get_input( double t ) = 0;
			/// Get the output y of the neuron.
			virtual double get_output( double t ) = 0;

		protected:
			Model& model;
			vector< shared_ptr< synapse_t > > synapses;
		};

		/// Neuron activation function type.
		typedef double ( *activation_t )( double );

		/// Implements a leaky-integrator that performs an Euler integration
		/// step (internally) each time the get_output is called. This
		/// implementation ensures that if one requests the output of a
		/// particular neuron, then all input neurons are also advanced in
		/// time. In other words, if one requires the output of a motor neuron,
		/// that depends on the output of other neurons, then the output of
		/// these neurons are automatically calculated and cached properly. The
		/// implementation is defined below:
		///
		///   tau * dV / dt = -V + I
		///
		///   y = f(V)
		///
		/// where, V is the membrane potential, I is the input current, tau the
		/// time constant, y the output of the neuron and f the activation
		/// function. The input current I is calculated as follows:
		///
		/// I = w_0 + \sum_j w_j * y_i
		///
		/// where, w_0 [-inf, inf] is the bias term, w_j the presynaptic control
		/// [-inf, inf], and y_j is the outputs of the neurons that connect to
		/// this neuron.
		struct leaky_neuron_t : public neuron_t
		{
			double tau; // time constant (can be set as property)
			double w0; // bias
			activation_t f; // activation function

			leaky_neuron_t( Model& model, double tau, double w0,
							activation_t f );
			virtual double get_input( double t ) override;
			virtual double get_output( double t ) override;

		protected:
			struct state_t
			{
				double t;
				double value;
			};
			state_t state;
			vector< state_t > state_history;

			state_t get_state( );
			void set_state( state_t new_state );
			double calc_derivative( double t, double v, double u );
			void euler_advance( double tf );
			void runge_kutta_advance( double tf );
		};

		/// Sensor neuron type. Gets muscles related sensory information from
		/// SCONE. For now, only muscle fiber length, velocity, and force
		/// sensors are supported. The input of the sensor uses the following
		/// convention:
		///
		///   I = w0 + s * g(t - dt)
		///
		/// where, s is the sensor scaling factor and g(t - dt) is the delayed
		/// value of the afferent. In our case, g can be either normalized fiber
		/// length, velocity or muscle force. The output of the neuron is
		/// governed by the differential equation of the base neuron
		/// (leaky-integrator).
		struct sensor_neuron_t : public leaky_neuron_t
		{
			/// neuron scale factor
			sensor_neuron_t( Model& model, double tau, double w0,
							 activation_t f, double s, TimeInSeconds delay,
							 SensorDelayAdapter* sensor );
			double get_input( double t ) override;

		protected:
			double s;
			TimeInSeconds delay;
			SensorDelayAdapter* sensor;
		};

		/// Inter neuron type.
		struct inter_neuron_t : public leaky_neuron_t
		{
			using leaky_neuron_t::leaky_neuron_t;
		};

		/// Motor neuron type.
		struct motor_neuron_t : public leaky_neuron_t
		{
			using leaky_neuron_t::leaky_neuron_t;
		};

		/// An inter neuron that implements a proportional logic of the form:
		///
		/// I = w0 + kp (w0 y_0 - p0) + kd (w1 y_1 - v0) + sum_2 w_i y_i
		///
		/// where kp, kd, p0, v0 are constants and y_0, y_1 are the two input to
		/// this neuron. Here we assume that the first synapse is a DoF position
		/// sensor (SN_DP) and the second a DoF velocity sensory (SN_DV). We can
		/// also create other synapses but we have to respect the first
		/// two. This is commonly used to implement proportional-derivative
		/// relationships for root (pelvis) stability.
		struct proportional_derivative_neuron_t : public inter_neuron_t
		{
			proportional_derivative_neuron_t( Model& model, double tau,
											  double w0, activation_t f,
											  double kp, double p0, double kd,
											  double v0 );
			double get_input( double t ) override;

		protected:
			double kp, p0, kd, v0;
		};

		/// Feed-forward state-dependent input neuron. The input of this neuron
		/// is defined by:
		///
		/// I = w0 + wp[phi]
		///
		/// where, wp contains the weight for a specific phase determined by phi
		/// (0 <= phi <= 4). This works only in combination with SCONE's
		/// GaitStateController. This will be deprecated because we will not be
		/// using he GaitStateController.
		struct feed_forward_neuron_t : public inter_neuron_t
		{
			feed_forward_neuron_t( Model& model, double tau, double w0,
								   activation_t f, string leg_identifier,
								   vector< double > phase_weights );
			double get_input( double t ) override;

		protected:
			string leg_identifier;
			vector< double > phase_weights;
		};

		/// Pattern-formation neuron.
		///
		/// The input of this neuron is defined by:
		///
		/// I = w0 + f(phi)
		///
		/// where, f is a pattern function (e.g., Bezier curve) and phi is a
		/// state variable. Internally, we clamp phi between [0, 2 pi]. This is
		/// intended to be used in combination with a phase oscillator, but the
		/// state variable can be anything (e.g., knee_angle_r).
		struct pattern_formation_neuron_t : public inter_neuron_t
		{
			pattern_formation_neuron_t( Model& model, double tau, double w0,
										activation_t f, string oscillator_state,
										FunctionUP& pattern );
			double get_input( double t ) override;

		protected:
			string oscillator_state;
			FunctionUP& pattern;
		};

		/// A spinal cord segment contains neurons and synapses.
		struct segment_organization_t
		{
			/// neurons are indexed by a the muscle and neuron type
			map< neuron_identifier_t, shared_ptr< neuron_t > > neurons;
			map< neuron_identifier_t, shared_ptr< neuron_t > > sensor_neurons;
			map< neuron_identifier_t, shared_ptr< neuron_t > > inter_neurons;
			map< neuron_identifier_t, shared_ptr< neuron_t > > motor_neurons;
			/// synapses are indexed by a synapse identifier
			map< synapse_identifier_t, shared_ptr< synapse_t > > synapses;
			/// Checks if neuron already exists and if not it adds it to this
			/// segment (in the corresponding placeholder).
			void
			ensure_neuron_exists( const neuron_identifier_t& neuron_identifier,
								  shared_ptr< neuron_t >& neuron );
			/// Adds a synapse between two neurons.
			void add_connection( const PropNode& props, Params& par,
								 const segment_t& segment,
								 const neuron_identifier_t& source,
								 const neuron_identifier_t& destination,
								 const neuron_action_t& neuron_action );
			/// Prune a neuron, related synapses and parameters from the model
			void prune_neuron( const PropNode& props, Params& par,
							   const neuron_identifier_t& neuron_identifier );
			/// Creates a graphviz file .gv for visualizing the neurons and
			/// their connectivity.
			void export_graph( string file_name );
		};

		/// The spinal cord model is a collection of spinal cord segments.
		struct spinal_cord_organization_t
		{
			/// spinal cord segments
			map< segment_t, segment_organization_t > segments;
			/// sensor neurons are kept outside the spinal cord because they are
			/// assumed unique to all segments (this might change)
			map< neuron_identifier_t, shared_ptr< neuron_t > > sensors;
			/// pattern formation primitives
			std::vector< FunctionUP > patterns;
			/// The logic that translates the SCONE's description language into
			/// a topological network of interconnected neurons.
			void build_reflex_network(
				const PropNode& props, Params& par, Model& model,
				const PropNode& neural_delays,
				const muscle_organization_t& muscle_organization,
				const reflex_rules_t& reflex_rules );
			/// Handling of pattern formation network is different
			/// from the reflex network.
			void build_pattern_network( const PropNode& props, Params& par,
										Model& model );
			/// Should be called after all neurons are added. This is
			/// used to establish connection between neurons that do
			/// not follow the ReflexRules.
			void build_direct_connections( const PropNode& props, Params& par,
										   Model& model );
			/// Should be called to create a set of rules to regulate root
			/// (pelvis) stability
			void build_root_stability( const PropNode& props, Params& par,
									   Model& model,
									   const PropNode& sensory_delay );

		private:
			void ensure_neuron_exists(
				const PropNode& props, Params& par, Model& model,
				const PropNode& neural_delays, const segment_t& segment,
				const neuron_identifier_t& neuron_identifier );
			void establish_relationship(
				const PropNode& props, Params& par, Model& model,
				const PropNode& neural_delays, const segment_t& segment,
				const neuron_identifier_t& source_identifier,
				const neuron_identifier_t& destination_identifier,
				const neuron_action_t& neuron_action );
		};
	}; // namespace spinal_cord

	/**************************************************************************/
	// The actual SCONE controller
	/**************************************************************************/

	/// A muscle excitation controller that realized the muscle organization and
	/// reflex rules that define the spinal cord model. Example of the syntax:
	/** Use case example (must be updated):
	\verbatim

	SegmentalController {
		# properties
		generate_graph = false
		sensory_delay_file = neural_delay_testing.txt
		# parameters
		bias = 0.0~0.1<-1,1>
		excitatory_synapse = 0.1~0.1<0,2>
		inhibitory_synapse = -0.1~0.1<-2,0>
		sensor_scale = 1.0
		length_offset = 1~0.1<-2,0.5>
		# velocity_offset = 0.0
		force_offset = 0.0
		# reflex rules
		ReflexRules {
			ReflexRule {
				source_role = agonist
				source_neuron = SN_Ib
				action = inhibit
				destination_role = antagonist
				destination_neuron = MN
			}
			ReflexRule {
				source_role = extensor
				source_neuron = SN_Ib
				action = excite
				destination_role = extensor
				destination_neuron = MN
			}
			ReflexRule {
				source_role = agonist
				source_neuron = SN_II
				action = excite
				destination_role = agonist
				destination_neuron = MN
			}
		}
		# muscle organization information
		SpinalCordOrganization {
			Organization {
				agonist = gastroc
				synergists = [soleus]
				antagonists = [tib_ant]
				segments = [S1 S2]
				extensor = true
			}
			Organization {
				agonist = soleus
				synergists = [gastroc]
				antagonists = [tib_ant]
				segments = [L5 S1 S2]
				extensor = true
			}
			Organization {
				agonist = tib_ant
				antagonists = [soleus gastroc]
				segments = [L4 L5 S1]
			}
		}
	}

	\endverbatim
	*/
	class SegmentalController : public Controller
	{
	public:
		/// Generates the topological network of the controller as .gv files for
		/// each spinal cord segment; default false.
		mutable bool generate_graph;

		/// Optional file containing the sensory delay values; if not provided
		/// delays are assumed 0.
		xo::path sensory_delay_file;

		SegmentalController( const PropNode& props, Params& par, Model& model,
							 const Location& loc );

	protected:
		bool ComputeControls( Model& model, double timestamp ) override;
		void StoreData( Storage< Real >::Frame& frame,
						const StoreDataFlags& flags ) const override;

	private:
		PropNode sensory_delay;
		spinal_cord::spinal_cord_organization_t spinal_cord;
	};

	/// Phase oscillator as proposed in Aoi et al. 2010 with adaptation to
	/// dynamically adjust omega.
	///
	/// dot_phi_left  = omega + delta_omega - gain * sin(phi_left  - phi_right -
	/// pi) dot_phi_right = omega + delta_omega - gain * sin(phi_right -
	/// phi_left - pi)
	///
	/// where omega is the frequency (slope of the solution). This component
	/// monitors the left and right contact forces. If a contact is detected,
	/// then the corresponding (left or right) phase variable phi is rested to
	/// phase_contact (default 0). The last mechanism is implemented through a
	/// discrete event function that monitors this condition.
	///
	/// delta_omega is computed by monitoring the step period and calculating an
	/// adjustment to delay or advance omega so that it is synchronized with the
	/// gait speed.
	///
	/// \verbatim
	///
	/// PhaseOscillator {
	///   omega = 5~0.1<0,10>
	///   initial_phase = [3.14 0]
	/// }
	///
	/// \endverbatim
	class PhaseOscillator : public StateComponent
	{
	public:
		PhaseOscillator( const PropNode& props, Params& par, Model& model );
		/// Contact delay (default 50ms).
		Real contact_delay;
		/// Stance threshold value (default 0.1).
		Real stance_load_threshold;
		/// Oscillator basic period (default 2 pi).
		Real omega;
		/// Coupling gain (default 1).
		Real gain;
		/// Initial phase [left, right] of the oscillator. The leg that touches
		/// the ground should receive 0.
		std::vector< Real > initial_phase;
		/// Toe off gait cycle percentage (default 0.6).
		Real toe_off_percentage;
		/// Synchronize swing (default true).
		bool synchronize_swing;
		/// Phase lock gain (default 0.5).
		Real phase_lock_gain;
		/// How many steps to ignore before phase locking is applied (default
		/// 2). You can set this to a very large number to prevent phase
		/// locking.
		int steps_ignore_phase_locking;

		/// Interface
		std::vector< String > GetStateNames( ) const override;
		std::vector< Real > GetInitialConditions( ) const override;
		std::vector< Real >
		CalcStateDerivatives( Real t, std::vector< Real > x0 ) const override;

	private:
		enum state_t
		{
			STANCE,
			SWING,
			UNKNOWN
		};
		mutable Real T_prev;
		mutable Real t_left, t_right;
		mutable Real delta_omega;
		mutable int steps;
		mutable state_t state_left, state_right;

		class HeelStrike : public StateComponent::DiscreteEvent
		{
		public:
			HeelStrike( PhaseOscillator* parent, state_t* state, Side side );
			int TriggeredOnSign( ) const;
			Real CheckForEvent( Real t, std::vector< Real > x ) const;
			std::vector< Real > EventHandler( Real t,
											  std::vector< Real > x ) const;

		protected:
			// pointers are references and they do not have to be deleted
			PhaseOscillator* oscillator;
			state_t* state;
			Side side;
		};

		void UpdateDeltaOmega( double t, Side side ) const;
		Real GetLoadSensorValue( state_t state, Side side ) const;
	};

	/// Phase-dependent pattern formation. This implements a feed-forward
	/// controller that defines N Bezier patterns which are fully connected with
	/// every muscle.
	///
	///   u = W * p(phi)
	///
	/// where u are the muscle excitations (symmetry), W is a weight matrix that
	/// maps pattern values to muscle excitations, and p are the patterns that
	/// are parameterized through a phase variable 0 <= phi <= 1. A
	/// PhaseOscillator is used to determine the phase of the gait in order to
	/// compute the value of each pattern. Symmetry is assumed between left and
	/// right leg muscles.
	///
	/// The intended use of this component is outside a SegmentalController,
	/// which has a similar feature. For example, we might want to use a
	/// GaitStateController and a feed-forward phase-dependent controller.
	///
	/// \verbatim
	///
	///    PatternFormationController {
	///        Patterns{
	///            Bezier {
	///                control_points = 5
	///                control_point_y = 0.5~0.5<0,1>
	///            }
	///            Bezier {
	///                control_points = 5
	///                Y2 = 2
	///            }
	///            Bezier {
	///                control_points = 5
	///                Y3 = 2~0.1<0,2>
	///            }
	///        }
	///        pattern_muscle_weight = 0.01~0.1<0,1>
	///    }
	///
	/// \endverbatim
	class PatternFormationController : public Controller
	{
	public:
		PatternFormationController( const PropNode& props, Params& par,
									Model& model, const Location& loc );

		/// Muscles to include; default = *.
		xo::pattern_matcher include_muscles;

	protected:
		bool ComputeControls( Model& model, double timestamp ) override;
		void StoreData( Storage< Real >::Frame& frame,
						const StoreDataFlags& flags ) const override;

	private:
		std::vector< FunctionUP > patterns;
		std::vector< std::map< String, double > > weights; // [pattern][muscle]
		std::pair< double, double > phi_cache;
	};

}; // namespace scone
